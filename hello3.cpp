#include <iostream>

class Cat {
public:
    void sayHello() {
        std::cout << "meow" << std::endl;
    }
} ;

int main() {
    Cat myCat;
    myCat.sayHello();
    return 0;
}